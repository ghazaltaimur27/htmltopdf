'use strict';
var fs = require('fs');
var pdf = require('dynamic-html-pdf');
const config = require('../../server/config');
const HtmlUrl = global.__basedir + '/../templates/html/Authorization-For-Release.html';
const PdfUrl = global.__basedir + '/../templates/pdf/Authorization-For-Release.pdf';

module.exports = function(Account) {
  Account.afterRemote('find', (ctx, _instance_, next) => {
    var baseDir =  HtmlUrl;
    var savePath = PdfUrl;

    var html = fs.readFileSync(baseDir, 'utf8');

    var options = {
      format: 'A4',
      orientation: 'portrait',
      border: {
        'top': '0',
        'right': '4mm',
        'bottom': '2mm',
        'left': '4mm',
      },
    };

    var document = {
      template: html,
      context: {
        data: ctx,
      },
      path: savePath,
    };

    pdf.create(document, options)
    .then(res => {
      console.log(res);
      return res;
    })
    .catch(error => {
      return error;
    });
  });
};
